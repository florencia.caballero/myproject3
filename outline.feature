Feature:outline
Scenario: Extraer dinero con distintas claves de tarjeta.
Given La tarjeta de crédito está habilitada
And El saldo disponible en mi cuenta es positivo
And El cajero tiene suficiente dinero
When Introduzco la tarjeta en el cajero
And Ingreso los siguientes <pin> y obtengo el resultado <resultado> :
 Examples: 
   
    | pin  | resultado |
    | 1234 | Error     |  	
    | 9876 | OK        |  
