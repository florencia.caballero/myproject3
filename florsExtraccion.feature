Feature: Extracción de dinero 


Background: 
Given La tarjeta de crédito está habilitada
And El saldo disponible en mi cuenta es positivo 
And El cajero tiene suficiente dinero

Scenario: Como usuario existente y habilitado del cajero, 
quiero realizar una extracción para obtener dinero.

Given Me autentiqué con una tarjeta habilitada
And El saldo disponible en mi cuenta es positivo
When Selecciono la opción de extraer dinero
And Ingreso la cantidad de dinero menor al saldo disponible y disponible del cajero 
Then Obtengo dinero 
And El dinero que obtuve se resta del saldo disponible de mi cuenta 
And El sistema devuelve la tarjeta automáticamente
And El sistema muestra el mensaje de transacción  finalizada
